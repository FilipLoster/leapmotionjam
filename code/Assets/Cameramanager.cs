﻿using UnityEngine;
using System.Collections;

public class Cameramanager : MonoBehaviour {

    GameObject secCam;
    Vector3 startRot;
    float timeRot;
    public float rotSpeed = 1.0f;
    public float turnAngle = 30.0f;

	// Use this for initialization
	void Start () {
        secCam = transform.FindChild("TS_Securit").gameObject;
        startRot = secCam.transform.localRotation.eulerAngles;

	}
	
	// Update is called once per frame
	void Update () {
        timeRot += Time.deltaTime * rotSpeed;
        secCam.transform.localRotation = Quaternion.Euler(startRot.x, 
            startRot.y + Mathf.Sin(Mathf.PI * 2 * timeRot) * turnAngle, 
            startRot.z);
	}
}
