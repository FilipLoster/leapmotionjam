﻿using UnityEngine;
using System.Collections;

public class ColorButton : MonoBehaviour {

    public GameManager manager;
    public Color32 color;
    public float ActivationTime = 0.5f;

    private float _initialOffset;
    private float _currentTime;
    private bool _isTriggered;
    private bool _clickRegistered;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    void Update() {
        if (!_isTriggered) {
            _currentTime -= Time.deltaTime;
            if (_currentTime < 0.0f) {
                _currentTime = 0.0f;
                _clickRegistered = false;
            }            
        }
    }


    void OnTriggerEnter(Collider other) {
        _isTriggered = true;
    }

    void OnTriggerExit(Collider other) {
        _isTriggered = false;
    }

    void OnTriggerStay(Collider other) {
        _currentTime += Time.deltaTime;
        if (_currentTime >= ActivationTime) {
            if (_clickRegistered == false) {
                _clickRegistered = true;
                manager.ColorButtonPressed(color);
            }
            _currentTime = ActivationTime;
        }        
    }
}
