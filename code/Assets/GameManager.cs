﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    public credits Credits;
    bool creditsEnabled = false;

    [Header("TV riddle")]
    public GameObject TVScreen;
    public TVPilot Pilot;
    bool pilotPressed = false;
    public Image newProgram;

    [Header("Mask riddle")]
    public GameObject[] MaskLights;
    public ColorButton[] Buttons;
    public GameObject Hologram;
    public Color32[] CorrectPattern;
    List<Color32> EnabledLights = new List<Color32>();

    [Header("Equations riddle")]
    public Text PinCounter;
    public GameObject[] EquationsPinPad;
    public GameObject EquationsDigitDisplay;
    public GameObject RiddleImageRevealer;
    public float RiddleImageRevealerYPosition;
    bool eqRiddleSolved = false;

    [Header("Colors riddle")]
    public GameObject FirstCircleAnswer;    
    //public 

    [Header("Hologram")]
    public GameObject HologramImage;
    private bool IsHologramEnabled;

    [Header("Final safe")]
    public GameObject Door;
    public Text FinalPin;
    bool finalRiddleSolved = false;

    public AudioSource bell;
    public AudioSource click;
    public AudioSource steps;
    public AudioSource puff;


    void Awake()
    {
        Instance = this;
        newProgram.gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
        int c = 0;
        foreach (var b in Buttons) {
            b.manager = this;            
        }

        RiddleImageRevealerYPosition = RiddleImageRevealer.transform.localPosition.y;
        DisableMaskLights();
        DisableHologram();
        DisableColorRiddleAnswers();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}
    
    public void RevealTheImageRiddle() {
        bell.Play();
        RiddleImageRevealer.transform.localPosition.Set(
            RiddleImageRevealer.transform.localPosition.x,
            RiddleImageRevealerYPosition - 4,
            RiddleImageRevealer.transform.localPosition.z
            );
    }

    void DisableMaskLights() {
        foreach (var l in MaskLights) {
            l.gameObject.SetActive(false);
        }
    }

    void DisableHologram() {
        HologramImage.SetActive(false);
        IsHologramEnabled = false;
    }

    void EnableHologram() {
        bell.Play();
        HologramImage.SetActive(true);
        IsHologramEnabled = true;
    }

    void DisableColorRiddleAnswers() {
        FirstCircleAnswer.SetActive(false);
    }

    public void GoToCredits() {
        if(!creditsEnabled) {
            bell.Play();
            creditsEnabled = true;
            Credits.enable();
        }        
    }

    public void PinPadPressedEquationsRiddle(int number) {
        if (eqRiddleSolved) return;

        click.Play();
        PinCounter.text += number.ToString();
        if (PinCounter.text.Length >= 4)
        {
            int val = Int16.Parse(PinCounter.text);
            if(val != 2634) {
                PinCounter.text = "";
                puff.Play();
            }
            else
            {
                bell.Play();
                eqRiddleSolved = true;
                RiddleImageRevealer.gameObject.SetActive(true);

            }
        }
    }

    public void SafePinButtonEntered(int number)
    {
        if (finalRiddleSolved) return;

        click.Play();
        FinalPin.text += number.ToString();
        if (FinalPin.text.Length >= 4) {
            int val = Int16.Parse(FinalPin.text);
            if(val != 9089) {
                FinalPin.text = "";
                puff.Play();
            } else {
                bell.Play();
                finalRiddleSolved = true;
                GameManager.Instance.GoToCredits();
            }
        }
    }

    public void TVPilotPressed()
    {
        if (pilotPressed) return;
        pilotPressed = true;
        bell.Play();
        click.Play();

        newProgram.gameObject.SetActive(true);
    }

    public void ColorButtonPressed(Color32 color) {
        if (IsHologramEnabled) return;

        click.Play();

        EnabledLights.Add(color);
        MaskLights[EnabledLights.Count - 1].gameObject.SetActive(true);
        MaskLights[EnabledLights.Count - 1].GetComponent<Light>().color = color;

        if (EnabledLights.Count >= 6) {
            bool correct = true;
            //verify pattern
            for (int i = 0; i < 6; i++) {                
                if (!CorrectPattern[i].Equals(EnabledLights[i])) correct = false;
            }

            if (correct) EnableHologram();
            else {
                puff.Play();
                EnabledLights.Clear();
                foreach(var l in MaskLights) {
                    l.gameObject.SetActive(false);
                }
            }
        } 
        
    }

    public Color32 ToColor(int HexVal)
    {        
        byte R = (byte)((HexVal >> 16) & 0xFF);
        byte G = (byte)((HexVal >> 8) & 0xFF);
        byte B = (byte)((HexVal) & 0xFF);
        return new Color32(R, G, B, 255);
    }
}
