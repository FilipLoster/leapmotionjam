﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class GameController : SceneSingleton<GameController>
{
    public static void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
