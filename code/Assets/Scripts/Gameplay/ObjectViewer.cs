﻿using UnityEngine;
using System.Collections;
using Leap;
using DG.Tweening;

public class ObjectViewer : MonoBehaviour
{
    public enum SwipeDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    public static ObjectViewer Instance;
    public Transform ViewTransform;
    public float RotationTime = 1.0f;
    public SpotActionButton StopButton;

    private float _cooldown;
    private GameObject _currentObject;
    private Vector3 _initialPosition;
    private Quaternion _initialRotation;
    private Transform _initialParent;
    private Transform _currentObjectTransform;
    private HandController _handController;
    private Controller _controller;
    private long _lastFrameId = -1;

    public GameObject Obj;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _handController = GetComponent<HMRConfigurationManager>()._handController;
        _controller = _handController.GetLeapController();
    }

    void OnEnable()
    {
        StopButton.gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        Instance = null;
    }

    void Update()
    {
        _cooldown -= Time.deltaTime;
        if(_cooldown < 0.0f)
        {
            _cooldown = 0.0f;
        }

        if(Input.GetKeyDown(KeyCode.Z))
        {
            ViewObject(Obj);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            StopViewing();
        }

        if (_currentObject == null || _controller == null || !_controller.IsConnected || !_controller.IsServiceConnected())
        {
            return;
        }

        var frame = _controller.Frame();

        if (frame == null)
        {
            return;
        }

        if(frame.Id == _lastFrameId)
        {
            return;
        }

        _lastFrameId = frame.Id;

        if (_currentObject != null)
        {
            var hand = frame.Hands.Frontmost;
            const float scale = 2.0f;
            var s = _currentObjectTransform.localRotation;
            var e = Quaternion.Euler(scale * Mathf.Rad2Deg * hand.Direction.Yaw, scale * Mathf.Rad2Deg * hand.PalmNormal.Roll, scale * Mathf.Rad2Deg * hand.Direction.Pitch);
            if(Quaternion.Angle(s, e) > 10.0f)
            {
                _currentObjectTransform.localRotation = Quaternion.Lerp(s, e, Mathf.Repeat(Time.deltaTime, RotationTime)/RotationTime);
            }
        }
    }

    public void ViewObject(GameObject obj)
    {
        if(_currentObject != null)
        {
            StopViewing();
        }
        _currentObject = obj;
        _currentObjectTransform = _currentObject.transform;
        _initialPosition = _currentObjectTransform.position;
        _initialRotation = _currentObjectTransform.rotation;
        _initialParent = _currentObjectTransform.parent;

        _currentObjectTransform.SetParent(ViewTransform);
        _currentObjectTransform.transform.localPosition = Vector3.zero;
        _currentObjectTransform.localRotation = Quaternion.identity;

        StopButton.gameObject.SetActive(true);
    }

    public void StopViewing()
    {
        if(_currentObject == null)
        {
            return;
        }
        _currentObjectTransform.SetParent(_initialParent);
        _currentObjectTransform.position = _initialPosition;
        _currentObjectTransform.rotation = _initialRotation;
        _currentObject = null;

        StopButton.gameObject.SetActive(false);
    }
}
