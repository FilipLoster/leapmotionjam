﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.VR;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public Spot CurrentSpot;
    public Transform CameraTransform;

    void Awake()
    {
        Instance = this;
        InputTracking.Recenter();
    }

    void Start()
    {
        transform.position = CurrentSpot.transform.position;
        transform.rotation = CurrentSpot.transform.rotation;
        CurrentSpot.FadeIn();
    }

    void OnDestroy()
    {
        Instance = null;
    }

    public void Move(Spot destination)
    {
        if(CurrentSpot != null)
        {
            CurrentSpot.FadeOut();
        }

        GameManager.Instance.steps.Play();

        CurrentSpot = destination;
        Sequence mySequence = DOTween.Sequence();
        var camRot = CameraTransform.localRotation;
        //mySequence.Append(transform.DORotate(Quaternion.LookRotation(destination.transform.position - CameraTransform.position).eulerAngles, 0.5f).SetEase(Ease.InOutCubic));
        mySequence.Append(transform.DOMove(destination.transform.position, 1.5f).SetEase(Ease.InOutCubic)).
        //mySequence.Append(transform.DORotate((destination.transform.rotation).eulerAngles, 0.5f).SetEase(Ease.InOutCubic)).
            OnUpdate(() =>
            {

            }).
            OnComplete(() =>
            {                
                CurrentSpot.FadeIn();
            });
    }
}
