﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

public class Spot : MonoBehaviour 
{
    public const float ConnectionButtonCameraOffset = 0.35f;

    public List<Spot> Connections;
    [HideInInspector]
    public List<SpotActionButton> Actions;

    private List<SpotConnectionButton> _connectionButtons = new List<SpotConnectionButton>();

    void Awake()
    {
        var prefab = Resources.Load<GameObject>("SpotConnectionButton");
        foreach(var connection in Connections)
        {
            var go = Instantiate<GameObject>(prefab);
            go.GetComponent<SpotConnectionButton>().Spot = connection;
            _connectionButtons.Add(go.GetComponent<SpotConnectionButton>());
            var gotr = go.transform;
            gotr.SetParent(transform);
            var p = transform.position;
            var dir = connection.transform.position - p;
            dir.Normalize();
            gotr.position = p + dir * ConnectionButtonCameraOffset;
            gotr.rotation = Quaternion.LookRotation(-dir);
        }

        Actions = new List<SpotActionButton>(GetComponentsInChildren<SpotActionButton>());

        FadeOut();
    }   

    void Update()
    {
        UpdateButtons();
    }

    public void FadeOut()
    {
        foreach(var button in _connectionButtons)
        {
            button.gameObject.SetActive(false);
        }
        foreach (var button in Actions)
        {
            button.gameObject.SetActive(false);
        }
    }

    public void FadeIn()
    {
        foreach (var button in _connectionButtons)
        {
            button.gameObject.SetActive(true);
        }
        foreach (var button in Actions)
        {
            button.gameObject.SetActive(true);
        }

        UpdateButtons();
    }

    void UpdateButtons()
    {
        var p = PlayerController.Instance.CameraTransform.position;
        for(int i=0; i<Connections.Count; ++i)
        {
            var button = _connectionButtons[i];
            var btr = button.transform;
            var connection = Connections[i];
            var dir = connection.transform.position - p;
            dir.Normalize();
            btr.position = p + dir * ConnectionButtonCameraOffset;
            btr.rotation = Quaternion.LookRotation(-dir);
        }
    }
}
