﻿using UnityEngine;
using System.Collections;

public class SpotConnectionButton : VRButton 
{
    public Spot Spot;

    protected override void OnClickInternal()
    {
        base.OnClickInternal();
        PlayerController.Instance.Move(Spot);
    }
}
