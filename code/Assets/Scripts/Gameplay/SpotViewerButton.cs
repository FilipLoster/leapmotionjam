﻿using UnityEngine;
using System.Collections;

public class SpotViewerButton : VRButton
{
    public GameObject Obj;

    protected override void OnClickInternal()
    {
        base.OnClickInternal();
        ObjectViewer.Instance.ViewObject(Obj);
    }
}
