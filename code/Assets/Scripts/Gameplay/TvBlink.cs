﻿using UnityEngine;
using System.Collections;

public class TvBlink : MonoBehaviour 
{
    public MeshRenderer MeshRenderer;

    public float Max = 1.0f;
    public float Min = 0.5f;
    public float Speed = 1.0f;
    public float Delay = 0.0f;

	void Update () 
    {
        //float emission = Min + Mathf.PingPong(Time.time*Speed*Random.Range(0.0f, Time.deltaTime/2.0f), Max - Min);
        //Debug.Log(emission);
        //Color baseColor = Color.white;
        //Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
        //MeshRenderer.material.SetColor("_EmissionColor", finalColor);
    }

    void Start()
    {
        StartCoroutine(Blink());
    }

    IEnumerator Blink()
    {
        yield return new WaitForSeconds(Delay);
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            float emission = Min + Mathf.PingPong(Time.time, Max - Min);
            Color baseColor = Color.white;
            Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
            MeshRenderer.material.SetColor("_EmissionColor", finalColor);
        }
    }
}
