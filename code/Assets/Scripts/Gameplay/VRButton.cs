﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class VRButton : MonoBehaviour 
{
    public UnityEvent OnClick;

    public GameObject Background;
    public GameObject Foreground;
    public float ActivationTime = 0.5f;

    private float _initialOffset;
    private float _currentTime;
    private bool _isTriggered;
    private bool _clickRegistered;
    private Transform _foregroundTransform;

    void Awake()
    {
        _foregroundTransform = Foreground.transform;
        _initialOffset = _foregroundTransform.transform.localPosition.z;
        OnClick.AddListener(OnClickInternal);
    }

    void OnEnable()
    {
        _clickRegistered = false;
        _currentTime = 0.0f;
        ResetForeground();
    }

    void OnDisable()
    {
        var p = _foregroundTransform.localPosition;
        _foregroundTransform.transform.localPosition = new Vector3(p.x, p.y, _initialOffset);
        _clickRegistered = false;
    }

    void OnDestroy()
    {
        OnClick.RemoveListener(OnClickInternal);
    }

    void Update()
    {
        if(!_isTriggered)
        {
            _currentTime -= Time.deltaTime;
            if(_currentTime < 0.0f)
            {
                _currentTime = 0.0f;
                _clickRegistered = false;
            }
            UpdateForeground();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        _isTriggered = true;
    }

    void OnTriggerExit(Collider other)
    {
        _isTriggered = false;
    }

    void OnTriggerStay(Collider other)
    {
        _currentTime += Time.deltaTime;
        if(_currentTime >= ActivationTime)
        {
            if(OnClick != null && _clickRegistered == false)
            {
                _clickRegistered = true;
                OnClick.Invoke();
            }
            _currentTime = ActivationTime;
        }
        UpdateForeground();
    }

    void UpdateForeground()
    {
        var p = _foregroundTransform.localPosition;
        _foregroundTransform.transform.localPosition = new Vector3(p.x, p.y, _initialOffset * ((ActivationTime - _currentTime) / ActivationTime));
    }

    void ResetForeground()
    {
        var p = _foregroundTransform.localPosition;
        _foregroundTransform.transform.localPosition = new Vector3(p.x, p.y, _initialOffset);
    }

    protected virtual void OnClickInternal()
    {
        
    }
}
