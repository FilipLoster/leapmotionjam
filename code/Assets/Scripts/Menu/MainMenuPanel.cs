﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuPanel : SceneSingleton<MainMenuPanel> 
{
    public void Play()
    {
        Application.LoadLevel("game_scene1");
    }

    public void Exit()
    {
        GameController.Exit();
    }
}
