﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour 
{
    [Serializable]
    public class ButtonPanelMapping
    {
        public Button Button;
        public MenuPanel Panel;
    }

    public ButtonPanelMapping BackMapping;
    public List<ButtonPanelMapping> NextMappings = new List<ButtonPanelMapping>();

	void Awake () 
    {
        if(BackMapping.Button != null && BackMapping.Panel != null)
        {
            BackMapping.Button.onClick.AddListener(Previous);
        }
        
        foreach(var m in NextMappings)
        {
            if(m.Button != null && m.Panel != null)
            {
                var panel = m.Panel;
                m.Button.onClick.AddListener(() => { Next(panel); });
            }
        }
	}

	void Update () 
    {
	    if(Input.GetKeyDown(KeyCode.Escape))
        {
            Previous();
        }
	}

    public void Previous()
    {
        if(BackMapping.Button == null && BackMapping.Panel == null)
        {
            return;
        }
        BackMapping.Panel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void Next(MenuPanel panel)
    {
        if(panel == null)
        {
            return;
        }
        panel.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
