﻿using UnityEngine;
using System.Collections;
using System;

public class SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = GetComponent<T>();

        }
        else if (_instance != this)
        {
            Destroy(this);
            throw new Exception(typeof(T).Name + ": instance already created");
        }
    }

    protected virtual void OnDestroy()
    {
        _instance = null;
    }
}
