﻿using System;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static bool _initialized = false;
    private static T _instance;

    public static T Instance
    {
        get
        {
            TryCreate();
            return _instance;
        }
    }

    public static void TryCreate()
    {
        if (!_initialized)
        {
            T[] instances = FindObjectsOfType<T>();

            if (instances != null && instances.Length > 0) throw new Exception(typeof(T).Name + ": unexpected instance found on scene!");

            GameObject prefab = Resources.Load(typeof(T).Name, typeof(GameObject)) as GameObject;
            if (prefab == null) throw new Exception(typeof(T).Name + ": prefab not found in Assets/Resources/.");

            GameObject holder = Instantiate(prefab) as GameObject;
            holder.name = typeof(T).Name + " (Singleton)";
            _instance = holder.GetComponent<T>() as T;
            if (_instance == null) throw new Exception(typeof(T).Name + ": stored singleton is missing script!");

            DontDestroyOnLoad(holder);

            _initialized = true;
            Debug.Log(typeof(T).Name + ": instance created.");
        }
    }

    public static void DestroyInstance()
    {
        if (_initialized)
        {
            _initialized = false;
            if (_instance != null) Destroy(_instance.gameObject);
        }
    }

#if UNITY_EDITOR

    //public virtual void OnValidate()
    //{
    //    UnityEngine.Object[] holders = Resources.LoadAll(typeof(T).Name, typeof(GameObject));

    //    if (holders != null || holders.Length != 1)
    //    {
    //        CreatePrefab();
    //    }
    //    else
    //    {
    //        Debug.LogWarning("Project can have only 1 prefab for " + typeof(T).Name + " singleton. Found: " + holders.Length);
    //    }
    //}

    [ContextMenu("Create prefab")]
    public void CreatePrefab()
    {
        PrefabUtility.CreatePrefab("Assets/Resources/" + typeof(T).Name + ".prefab", gameObject);
        Debug.Log("Created prefab for " + typeof(T).Name + " singleton.");
    }

#endif
}
