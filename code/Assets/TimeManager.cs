﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {

    Text timeLeft;
    public float timeLeftValue = 900;

	// Use this for initialization
	void Start () {
        timeLeft = transform.FindChild("Text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        timeLeftValue -= Time.deltaTime;
        int min = Mathf.FloorToInt(timeLeftValue / 60);
        int sec = (Mathf.FloorToInt(timeLeftValue % 60));
        timeLeft.text = min + ":" + (sec < 10 ? "0" : "") + sec;

        if (timeLeftValue < 0) {
            GameManager.Instance.GoToCredits();
        }
	}
}
