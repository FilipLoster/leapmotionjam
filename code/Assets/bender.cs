﻿using UnityEngine;
using System.Collections;

public class bender : MonoBehaviour {

    MeshFilter mFilter;
    public float bendAmmount = 20;

    void Awake() {
        mFilter = GetComponent<MeshFilter>();
    }

	// Use this for initialization
	void Start () {
        updateBend();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void updateBend() {
        float minX = mFilter.mesh.vertices[0].x;
        float maxX = mFilter.mesh.vertices[0].x;
        foreach (var v in mFilter.mesh.vertices) {
            minX = Mathf.Min(minX, v.x);
            maxX = Mathf.Max(maxX, v.x);
        }

        Vector3[] vertices = mFilter.mesh.vertices;
        int i = 0;
        foreach (var v in vertices) {
            float perc = (v.x - minX) / (maxX - minX);
            vertices[i] = new Vector3(
                vertices[i].x, 
                vertices[i].y + bendAmmount * Mathf.Sin(perc * Mathf.PI), 
                vertices[i].z);
            //mFilter.mesh.normals[i] * Mathf.Sin(Time.time);
            i++;
            
            //.Set(v.x, v.y, 30 + Mathf.Sin(Time.time) * 20);//v.z + bendAmmount * Mathf.Sin(perc * Mathf.PI));
        }
        mFilter.mesh.vertices = vertices;
    }
}
