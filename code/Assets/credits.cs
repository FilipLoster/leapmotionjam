﻿using UnityEngine;
using System.Collections;

public class credits : MonoBehaviour {

    public GameObject cam;
    public GameObject room;
    bool enabled = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (enabled) {
            transform.Translate(0.0f, Time.deltaTime * 10.0f, 0);

            if (transform.position.y > 900)
                Application.Quit();
        }
	}

    [ContextMenu("credits")]
    public void enable() {
        enabled = true;
        room.SetActive(false);
        cam.transform.rotation = Quaternion.EulerAngles(0, 0, 0);
        cam.transform.position = transform.position;
        cam.transform.Translate(0, 0, -200);
        cam.transform.LookAt(transform);
    }
}
