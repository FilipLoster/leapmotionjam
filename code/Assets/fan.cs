﻿using UnityEngine;
using System.Collections;

public class fan : MonoBehaviour {

    public float bladesRotatingSpeed;
    GameObject blades;

	// Use this for initialization
	void Start () {
        blades = transform.FindChild("wings").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        blades.transform.Rotate(new Vector3(0, bladesRotatingSpeed * Time.deltaTime, 0));
	}
}
